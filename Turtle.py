import turtle

UP = 1
RIGHT = 2
DOWN = 3
LEFT = 4

SEGMENT = 12
SHORT_SEGMENT = 3

def main():
  t = turtle.Turtle()
  write_message(t, 'abcdefghijklmnopqrstuvwxyz')

def follow_code(Turt, code):

  for instruction in code:
    if instruction == 'f':
      Turt.forward(SEGMENT)
    elif instruction == 'F':
      Turt.backward(SEGMENT)
    elif instruction == 'r':
      Turt.right(90)
    elif instruction == 'l':
      Turt.left(90)
    elif instruction == 'R':
      Turt.right(45)
    elif instruction == 'L':
      Turt.left(45)
    elif instruction == 'd':
      Turt.forward(SEGMENT*1.4)
    elif instruction == 'D':
      Turt.backward(SEGMENT*1.4)
    elif instruction == 'h':
      Turt.forward(SEGMENT/2)
    elif instruction == 'H':
      Turt.backward(SEGMENT/2)
    elif instruction == 's':
      Turt.forward(SHORT_SEGMENT)
    elif instruction == 'S':
      Turt.backward(SHORT_SEGMENT)

def reorient(Turt, curr, target):
  if curr == target:
    return
  if (curr == DOWN and target == UP) or (curr == UP and target == DOWN) or (curr == LEFT and target == RIGHT) or (curr == RIGHT and target == LEFT):
    Turt.right(180)
  elif (curr == DOWN and target == LEFT) or (curr == LEFT and target == UP) or (curr == UP and target == RIGHT) or (curr == RIGHT and target == DOWN):
    Turt.right(90)
  else:
    Turt.left(90) 

def add_space(Turt, direction, gap = 1):  
  reorient(Turt, direction, LEFT)
  
  follow_code(Turt, 'sSSslffl' + ('f' * gap) + 'lffrsSSsl')
  return UP

def draw_char(Turt, c, direction = RIGHT):
  reorient(Turt, direction, UP)
  
  if c == 'a' or c == 'A':
    follow_code(Turt, 'ffrfrfrfFlf')
    return DOWN, 1
  elif c == 'b' or c == 'B':
    follow_code(Turt, 'ffFrfrfrfF')
    return LEFT, 1
  elif c == 'c' or c == 'C':    
    follow_code(Turt, 'ffrfFrfflf')
    return RIGHT, 1
  elif c == 'd' or c == 'D':
    follow_code(Turt, 'frflfFFlfF')
    return LEFT, 1
  elif c == 'e' or c == 'E':
    follow_code(Turt, 'ffrfFrflfFrflf')
    return RIGHT, 1
  elif c == 'f' or c == 'F':
    follow_code(Turt, 'ffrfFrflfFrf')
    return DOWN, 2
  elif c == 'g' or c == 'G':
    follow_code(Turt, 'ffrfFrfflflflhHlf')
    return DOWN, 1
  elif c == 'h' or c == 'H':
    follow_code(Turt, 'ffFrflfFF')
    return UP, 1
  elif c == 'i' or c == 'I':
    follow_code(Turt, 'ffFF')
    return UP, 1
  elif c == 'j' or c == 'J':
    follow_code(Turt, 'hHrflffFF')
    return UP, 1
  elif c == 'k' or c == 'K':
    follow_code(Turt, 'ffFRdDrdDRf')
    return DOWN, 2
  elif c == 'l' or c == 'L':
    follow_code(Turt, 'ffFFrf')
    return RIGHT, 1
  elif c == 'm' or c == 'M':
    follow_code(Turt, 'frhrfFlhrf')
    return DOWN, 1
  elif c == 'n' or c == 'N':
    follow_code(Turt, 'frfrf')
    return DOWN, 1
  elif c == 'o' or c == 'O':
    follow_code(Turt, 'frfrfrf')
    return LEFT, 2
  elif c == 'p' or c == 'P':
    follow_code(Turt, 'ffrfrfrflf')
    return DOWN, 2
  elif c == 'q' or c == 'Q':
    follow_code(Turt, 'ffrfrffrfFRhHHhL')
    return LEFT, 1
  elif c == 'r' or c == 'R':
    follow_code(Turt, 'ffrfrfrflLdL')
    return RIGHT, 1
  elif c == 's' or c == 'S':
    follow_code(Turt, 'rflflfrfrfFlFlFrF')
    return UP, 1
  elif c == 't' or c == 'T':
    follow_code(Turt, 'ffFrhHrflfF')
    return RIGHT, 2
  elif c == 'u' or c == 'U':
    follow_code(Turt, 'fFrflfF')
    return UP, 1
  elif c == 'v' or c == 'V':
    follow_code(Turt, 'rhLfFlfFLh')
    return LEFT, 2
  elif c == 'w' or c == 'W':
    follow_code(Turt, 'fFrhlfFrhlfF')
    return UP, 1
  elif c == 'x' or c == 'X':
    follow_code(Turt, 'RffFlfFFR')
    return UP, 1
  elif c == 'y' or c == 'Y':
    follow_code(Turt, 'rflffFlfrfFrfrfrf')
    return LEFT, 2
  elif c == 'z' or c == 'Z':
    follow_code(Turt, 'RdLlfFLdRF')
    return LEFT, 1
  else:
    return UP, 2

def write_message(Turt, message, direction = RIGHT):
  for c in message:
    direction, gap = draw_char(Turt, c, direction)
    direction = add_space(Turt, direction, gap)

main()